<?php

use yii\db\Migration;

class m170716_151903_alter___status__table extends Migration
{
    public function safeUp()
    {
		$this->createTable('status', [
            'id' => 'pk',
			'name' => 'string',
        ],
		'ENGINE=InnoDB'
		);
    }

    public function safeDown()
    {
         $this->dropTable('status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170716_151903_alter___status__table cannot be reverted.\n";

        return false;
    }
    */
}
