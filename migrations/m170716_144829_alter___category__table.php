<?php

use yii\db\Migration;

class m170716_144829_alter___category__table extends Migration
{
    public function safeUp()
   {
        $this->createTable('category', [
            'id' => 'pk',
			'name' => 'string',
        ],
		'ENGINE=InnoDB'
		);
    }
	
    public function safeDown()
    {
       $this->dropTable('category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170716_144829_alter___category__table cannot be reverted.\n";

        return false;
    }
    */
}
