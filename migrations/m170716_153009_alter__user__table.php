<?php

use yii\db\Migration;

class m170716_153009_alter__user__table extends Migration
{
    public function safeUp()
    {
$this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
			'authKey' => $this->string()->notNull(),
        ]);
    }

   public function safeDown()
    {
       $this->dropTable('user');
        
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170716_153009_alter__user__table cannot be reverted.\n";

        return false;
    }
    */
}
