<?php

use yii\db\Migration;

class m170716_150959_alter___activity__table extends Migration
{
    public function safeUp()
    {
{
        $this->createTable('activity', [
            'id' => 'pk',
			'titale' => 'string',
			'categoryId' => 'integer',
			'statusId' => 'integer',
        ],
		'ENGINE=InnoDB'
		);
    }
    }

    public function safeDown()
    {
		  $this->dropTable('activity');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170716_150959_alter___activity__table cannot be reverted.\n";

        return false;
    }
    */
}
