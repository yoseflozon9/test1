<?php

use yii\db\Migration;

class m170718_160836_delete__role_column_to_user_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170718_160836_delete__role_column_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170718_160836_delete__role_column_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
